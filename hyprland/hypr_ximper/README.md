## HYPR XIMPER
<br />
  
<div align="center">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_ximper/.img/1.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_ximper/.img/2.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_ximper/.img/3.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_ximper/.img/4.jpg?ref_type=heads" width="550">
</div>
<br /><br />

## Привет!
Этот райс я настраивал под себя на дистрибутиве Ximper Linux. Софт уже был весь предустановлен, разве что cmus, cava, ranger... Основное в дистрибутиве уже есть. Для установки конфига достаточно скопировать файлы из .config в ~/.config с заменой конечно.

В дистрибутиве уже все предустановленно, wi-fi, bluetooth работают из коробки. Годный дистрибутив для попробовать тайлинг на примере Hyprland, но нужно учитывать что версия 0.9.3. и у юзернейма могут быть проблемы.

  
# ИНФО
|DISTRO|[XimperLinux](https://ximperlinux.ru/)|
| ------ | ------ |
|WM|[Hyprland](https://hyprland.org/)|
|BAR|[Waybar](https://github.com/Alexays/Waybar)|
|LAUNCHER|[Wofi](https://sr.ht/~scoopta/wofi/)|
|TERMINAL|[Kitty](https://sw.kovidgoyal.net/kitty/)|
|SHELL|[Fish](https://fishshell.com/)|
|ICON|[Delight](https://www.pling.com/p/1532276)|
|GTK3|[Adwaita-Dark](---)|
|CURSORS|[Adwaita](---)|
|FONT|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|WALL_1|[Тут](https://sun9-39.userapi.com/impg/ZGwwsIt8rfpePcHIQB4TVb1gPhTAb5zKo5x1oA/jIRmd6tYOKA.jpg?size=2560x1440&quality=95&sign=bf497787c5c9fbd258f0edd97f7ea418&type=album)|
  
## НАСТРОЙКА СИСТЕМЫ
- [```Установка VoidLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-uefi-install)  
- [```Установка ArchLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/ArchLinux-uefi-install)  
- [```Автостарт и Автологин```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Autostart_wm)  
- [```Установка Apparmor```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Apparmor)  
- [```Установка Lutris```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Lutris)  
- [```Установка Virt-manager```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Virt-Manager)  
- [```VoidLinux Wayland WM```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-Wayland-WM)   
- [```Установка Pipewire```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Pipewire)   


## ССЫЛКИ
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_youtube.png?ref_type=heads" width="100">](https://www.youtube.com/@prolinux2753)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_tg.png?ref_type=heads" width="100">](https://t.me/prolinux_tg)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_unsplash.png?ref_type=heads" width="100">](https://unsplash.com/@owl410/collections)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_coffee.png?ref_type=heads" width="100">](https://www.donationalerts.com/r/prolinux)

